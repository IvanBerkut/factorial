var numberInput = document.querySelector('.main_input-number');
var factorialOutput = document.querySelector('.main_result_field');
var convertForm = document.querySelector('.convert-form');

convertForm.addEventListener('submit', function (evt) {
  evt.preventDefault();
  var numberValue = numberInput.value;
  var factorial = 1;
  for (var i = numberValue; i > 0; i--){
    factorial = factorial * i;
  };
  factorialOutput.textContent = numberValue + "! = " + factorial;
  numberInput.value = '';
});
